package com.addcel.tebca.client.model;

import java.util.List;

public class HistoryTransactions {
	
	private String totalTransactions;
	private String rc;
	private String msg;
	private String transactionDate;
	private List<NovoTransaction> transactions;

	public HistoryTransactions() {
		// TODO Auto-generated constructor stub
	}

	public String getTotalTransactions() {
		return totalTransactions;
	}

	public void setTotalTransactions(String totalTransactions) {
		this.totalTransactions = totalTransactions;
	}

	public String getRc() {
		return rc;
	}

	public void setRc(String rc) {
		this.rc = rc;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public List<NovoTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<NovoTransaction> transactions) {
		this.transactions = transactions;
	}
	
}
