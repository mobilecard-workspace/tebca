package com.addcel.tebca.client.model;

public class SendMoneyRes {
	
	private String rc;
	private String msg;
	private String transactionDate;
	private Reference transaction;
	
	
	public SendMoneyRes() {
		// TODO Auto-generated constructor stub
	}

	public String getRc() {
		return rc;
	}

	public void setRc(String rc) {
		this.rc = rc;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Reference getTransaction() {
		return transaction;
	}

	public void setTransaction(Reference transaction) {
		this.transaction = transaction;
	}

}
