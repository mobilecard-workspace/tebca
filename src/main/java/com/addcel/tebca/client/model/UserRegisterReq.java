package com.addcel.tebca.client.model;

public class UserRegisterReq {
	
	
	private String tagpay;
	private String documentId;
	private String documentTypeId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String surName;
	private String checkDigit;
	private String birthday;
	private String nationality;
	private String countryResidence;
	private String gender;
	private String address;
	private String regionId;
	private String locationId;
	private String subLocationId;
	private String phoneNumber;
	private String email;
	private String workCenter;
	private String occupation;
	private String civilServant;
	private String publicOffice;
	private String publicInstitution;
	private String agreementAccept;
	private String acceptProtectData;
	
	
	public UserRegisterReq() {
		// TODO Auto-generated constructor stub
	}

	
	public String getTagpay() {
		return tagpay;
	}
	/**
	 * 
	 * @param tagPay Campo para almacenar el alias (tagPay) del usuario que se desea verificar como único en la aplicación.
	 */
	public void setTagpay(String tagpay) {
		this.tagpay = tagpay;
	}
	
	public String getDocumentId() {
		return documentId;
	}

	/**
	 * 
	 * @param documentId Campo que identifica el documento de identidad del usuario
	 */
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentTypeId() {
		return documentTypeId;
	}

	/**
	 * 
	 * @param documentTypeId Campo que identifica el id del tipo de documento de identidad del usuario 1 = DNI 2= Pasaporte
	 */
	public void setDocumentTypeId(String documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public String getFirstName() {
		return firstName;
	}

	/**
	 * 
	 * @param firstName Campo que identifica el primer nombre del usuario
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	/**
	 * 
	 * @param middleName Campo que identifica el segundo nombre del usuario
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * @param lastName Campo que identifica el primer apellido del usuario
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurName() {
		return surName;
	}

	/**
	 * 
	 * @param surName Campo que identifica el segundo apellido del usuario
	 */
	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	/**
	 * 
	 * @param checkDigit Ultimo número del documento de identidad que identifica el digito verificador asociado al usuario
	 */
	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

	public String getBirthday() {
		return birthday;
	}

	/**
	 * 
	 * @param birthday Campo que identifica la fecha de nacimiento del usuario en formato DD/MM/AAAA 01/01/1990
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getNationality() {
		return nationality;
	}

	/**
	 * 
	 * @param nacionality Campo que identifica el país de nacimiento del usuario, bajo el estandar ISO 3166-2 PE(peru)
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	
	public String getCountryResidence() {
		return countryResidence;
	}
	/**
	 * 
	 * @param residenceCountry Campo que identifica el país de nacimiento del usuario, bajo el estandar ISO 3166-2
	 */
	public void setCountryResidence(String countryResidence) {
		this.countryResidence = countryResidence;
	}

	public String getGender() {
		return gender;
	}

	/**
	 * 
	 * @param gender Campo que identifica el género (femenino o masculino) del usuario
	 * F= Femenino
	 * M= Masculino
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	/**
	 * 
	 * @param address Campo que identifica la dirección específica del usuario
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	public String getRegionId() {
		return regionId;
	}

	/**
	 * 
	 * @param regionId Campo que identifica el ID del departamento en el que reside el usuario Tabla N° 2: Códigos de regiones
	 */
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getLocationId() {
		return locationId;
	}

	/**
	 * 
	 * @param locationId Campo que identifica el ID de la provincia en la que reside el usuario Tabla N° 2: Códigos de regiones
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getSubLocationId() {
		return subLocationId;
	}

	/**
	 * 
	 * @param subLocationId Campo que identifica el ID del distrito en la que reside el usuario. Tabla N° 2: Códigos de regiones
	 */
	public void setSubLocationId(String subLocationId) {
		this.subLocationId = subLocationId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * 
	 * @param phoneNumber Campo para almacenar el número de teléfono del usuario, que se desea verificar como en la aplicación.
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email Campo para almacenar la dirección de correo del usuario, que se desea verificar como único en la aplicación.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	public String getWorkCenter() {
		return workCenter;
	}

	/**
	 * 
	 * @param worckCenter Campo que indica el centro laboral o razón social de la empresa en la que trabaja el usuario. En caso de que el usuario no labore, debe indicar su condición en este campo.
	 */
	public void setWorkCenter(String workCenter) {
		this.workCenter = workCenter;
	}

	public String getOccupation() {
		return occupation;
	}

	/**
	 * 
	 * @param occupation Campo que indica la profesión u ocupación del usuario.
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getCivilServant() {
		return civilServant;
	}

	/**
	 * 
	 * @param civilServant Campo que indica si el usuario ejerce o ejerció algún cargo público. 0=NO 1= SI
	 */
	public void setCivilServant(String civilServant) {
		this.civilServant = civilServant;
	}

	public String getPublicOffice() {
		return publicOffice;
	}

	/**
	 * 
	 * @param publicOffice Campo que indica el cargo que ocupa en la institución pública, si el campo civilServant es 1 (true).
	 */
	public void setPublicOffice(String publicOffice) {
		this.publicOffice = publicOffice;
	}

	public String getPublicInstitution() {
		return publicInstitution;
	}

	/**
	 * 
	 * @param publicInstitution Campo que indica la institución pública, si el campo civilServant es 1 (true).
	 */
	public void setPublicInstitution(String publicInstitution) {
		this.publicInstitution = publicInstitution;
	}

	public String getAgreementAccept() {
		return agreementAccept;
	}

	/**
	 * 
	 * @param acceptContract Campo que indica la aceptación del contrato por parte del usuario. 0=NO 1=SI
	 */
	public void setAgreementAccept(String agreementAccept) {
		this.agreementAccept = agreementAccept;
	}

	public String getAcceptProtectData() {
		return acceptProtectData;
	}

	/**
	 * 
	 * @param acceptProtectData Campo que indica si el usuario ejerce o ejerció algún cargo público. 0=NO 1=SI
	 */
	public void setAcceptProtectData(String acceptProtectData) {
		this.acceptProtectData = acceptProtectData;
	}
	
	
	

}
