package com.addcel.tebca.client.model;

public class CardNovo {

	private String pan;
	private String expiryDate;
	private String name;
	
	
	public CardNovo() {
		// TODO Auto-generated constructor stub
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
