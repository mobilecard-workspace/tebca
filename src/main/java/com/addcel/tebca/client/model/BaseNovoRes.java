package com.addcel.tebca.client.model;

public class BaseNovoRes {
	
	private String rc;
	private String msg;
	private String transactionDate;
	
	public BaseNovoRes() {
		// TODO Auto-generated constructor stub
	}

	public String getRc() {
		return rc;
	}

	public void setRc(String rc) {
		this.rc = rc;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	
}
