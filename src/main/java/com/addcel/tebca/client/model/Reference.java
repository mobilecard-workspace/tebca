package com.addcel.tebca.client.model;

public class Reference {
	
	private String referenceNumber;
	private String requestId;
	
	public Reference() {
		// TODO Auto-generated constructor stub
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	
	

}
