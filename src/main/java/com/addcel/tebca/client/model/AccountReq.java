package com.addcel.tebca.client.model;

public class AccountReq {
	
	private String tagpay;
	private String issuerId;
	private String issuerBin;
	
	public AccountReq() {
		// TODO Auto-generated constructor stub
	}

	public String getTagpay() {
		return tagpay;
	}

	public void setTagpay(String tagpay) {
		this.tagpay = tagpay;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerBin() {
		return issuerBin;
	}

	public void setIssuerBin(String issuerBin) {
		this.issuerBin = issuerBin;
	}

}
