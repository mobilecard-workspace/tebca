package com.addcel.tebca.client.model;

public class AssignCardReq {
	
	private String tagpay;
	private String pan;
	private String expiryDate;
	private String action;
	
	public AssignCardReq() {
		// TODO Auto-generated constructor stub
	}

	public void setTagpay(String tagpay) {
		this.tagpay = tagpay;
	}
	
	public String getTagpay() {
		return tagpay;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * 
	 * @param expiryDate MMYY
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
}
