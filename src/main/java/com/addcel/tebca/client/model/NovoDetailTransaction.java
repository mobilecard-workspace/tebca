package com.addcel.tebca.client.model;

public class NovoDetailTransaction {

	private String referenceNumber;
	private String transactionState;
	private String transactionType;
	private String amount;
	private String calculatedCommission;
	private String total;
	private String lastFourDigitPan;
	private String date;
	private String fullName;
	private String tagpay;
	private String description;
	

	public NovoDetailTransaction() {
		// TODO Auto-generated constructor stub
	}


	public String getReferenceNumber() {
		return referenceNumber;
	}


	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}


	public String getTransactionState() {
		return transactionState;
	}


	public void setTransactionState(String transactionState) {
		this.transactionState = transactionState;
	}


	public String getTransactionType() {
		return transactionType;
	}


	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}


	public String getAmount() {
		return amount;
	}


	public void setAmount(String amount) {
		this.amount = amount;
	}


	public String getCalculatedCommission() {
		return calculatedCommission;
	}


	public void setCalculatedCommission(String calculatedCommission) {
		this.calculatedCommission = calculatedCommission;
	}


	public String getTotal() {
		return total;
	}


	public void setTotal(String total) {
		this.total = total;
	}


	public String getLastFourDigitPan() {
		return lastFourDigitPan;
	}


	public void setLastFourDigitPan(String lastFourDigitPan) {
		this.lastFourDigitPan = lastFourDigitPan;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public String getTagpay() {
		return tagpay;
	}


	public void setTagpay(String tagpay) {
		this.tagpay = tagpay;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
}
