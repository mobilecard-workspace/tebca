package com.addcel.tebca.client.model;

public class SendMoneyReq {
	
	private String tagpaySend;
	private String tagpayReceive;
	private String amount;
	private String description;
	private String commision;
	private String calculatedComission;
	
	
	public SendMoneyReq() {
		// TODO Auto-generated constructor stub
	}

	
	public String getCommision() {
		return commision;
	}


	public void setCommision(String commision) {
		this.commision = commision;
	}


	public String getCalculatedComission() {
		return calculatedComission;
	}


	public void setCalculatedComission(String calculatedComission) {
		this.calculatedComission = calculatedComission;
	}


	public String getTagpaySend() {
		return tagpaySend;
	}

	public void setTagpaySend(String tagpaySend) {
		this.tagpaySend = tagpaySend;
	}

	public String getTagpayReceive() {
		return tagpayReceive;
	}

	public void setTagpayReceive(String tagpayReceive) {
		this.tagpayReceive = tagpayReceive;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
