package com.addcel.tebca.client.model;

public class NovoTransaction {

	private String transactionType;
	private String description;
	private String amount;
	private String date;
	private NovoDetailTransaction detailTransaction;
	
	public NovoTransaction() {
		// TODO Auto-generated constructor stub
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public NovoDetailTransaction getDetailTransaction() {
		return detailTransaction;
	}

	public void setDetailTransaction(NovoDetailTransaction detailTransaction) {
		this.detailTransaction = detailTransaction;
	}
	
}
