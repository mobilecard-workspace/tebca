package com.addcel.tebca.services;

import com.addcel.tebca.client.model.Balance;
import com.addcel.tebca.client.model.BaseNovoRes;
import com.addcel.tebca.client.model.HistoryTransactions;
import com.addcel.tebca.client.model.TokenRes;
import com.addcel.tebca.controller.model.BaseResponse;
import com.addcel.tebca.controller.model.EnrollCardRes;
import com.addcel.tebca.controller.model.TransferReq;
import com.addcel.tebca.controller.model.TransferRes;


public interface TebcaService {

	TokenRes getToken();
	EnrollCardRes assignCardMC(long idUsuario, int idApp, int idPais, String idioma,String tipoUsuario);
	EnrollCardRes associateCardMC(long idUsuario, int idApp, int idPais, String idioma, String pan, String expiryDate, String codigo,String tipoUsuario );
	Balance getBalance(long idUsuario, int idApp, int idPais, String idioma,String tipoUsuario);
	BaseResponse accountLockStatus(long idUsuario, int idApp, int idPais, String idioma,String tipoUsuario, int proceso);
	HistoryTransactions getMovements(long idUsuario, int idApp, int idPais, String idioma,String tipoUsuario,String limit);
	BaseResponse CardReplacement(long idUsuario, int idApp,int idPais, String idioma, String tipoUsuario);
	TransferRes fundsTransfers(TransferReq req, int idApp,int idPais, String idioma);
	
}
