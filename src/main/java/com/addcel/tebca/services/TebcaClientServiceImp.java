package com.addcel.tebca.services;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.addcel.tebca.client.model.AccountReq;
import com.addcel.tebca.client.model.AccountRes;
import com.addcel.tebca.client.model.AssignCardReq;
import com.addcel.tebca.client.model.Balance;
import com.addcel.tebca.client.model.BaseNovoRes;
import com.addcel.tebca.client.model.Data;
import com.addcel.tebca.client.model.History;
import com.addcel.tebca.client.model.HistoryTransactions;
import com.addcel.tebca.client.model.SendMoneyReq;
import com.addcel.tebca.client.model.SendMoneyRes;
import com.addcel.tebca.client.model.TokenRes;
import com.addcel.tebca.client.model.UserRegisterReq;
import com.addcel.tebca.client.model.UserRegisterRes;
import com.addcel.tebca.client.model.JumioVerifyRes;
import com.google.gson.Gson;

@Service
public class TebcaClientServiceImp implements TebcaClientService{

	private static Logger LOGGER = LogManager.getLogger(TebcaClientServiceImp.class);

	/*PROD
	 */ 
	private final static String PATH_GET_TOKEN = "https://oa.novopayment.net/oauth2/1.0/token"; // pro "; https://d-oa.novopayment.net/oauth2/1.0/token
	private final static String PATH_REFRESH_TOKEN =  "https://oa.novopayment.net/oauth2/1.0/refresh";  // prod "https://oa.novopayment.net/oauth2/1.0/refresh";  "https://d-oa.novopayment.net/oauth2/1.0/refresh";
	private final static String PATH_USER_REGISTER = "https://peru.novopayment.net/core/enrolls/v1/user"; // prod  "https://d-peru.novopayment.net/core/enrolls/v1/user";
	private final static String PATH_CREATE_PREPAID_ACCOUNT = "https://peru.novopayment.net/core/accounts/v1/debitcard";   // prod  "https://d-peru.novopayment.net/core/accounts/v1/debitcard";				 
	private final static String PATH_TRANSACTIONS_HISTORY = "https://peru.novopayment.net/core/transactionshistory/v1/user/{tagpay}/debitcard/{subbin}/transactions/{limit}";// prob   "https://d-peru.novopayment.net/core/transactionshistory/v1/user/{tagpay}/debitcard/{subbin}/transactions/{limit}";
	private final static String PATH_CHECK_BALANCE = "https://peru.novopayment.net/core/accounts/v1/debitcard/user/{tagpay}/balance/{issuerBin}"; //prod  "https://d-peru.novopayment.net/core/accounts/v1/debitcard/user/{tagpay}/balance/{issuerBin}";// 	
	private final static String PATH_SEND_MONEY = "https://d-peru.novopayment.net/core/fundstransfer/v1/accounts/{type}/{requestId}" ;//"https://d-peru.novopayment.net/core/fundstransfer/v1/accounts?requestId={requestId}"; prod "https://peru.novopayment.net/core/fundstransfer/v1/accounts/{type}/{requestId}"
	private final static String PATH_BLOCK_UNBLOCK_ACCOUNT = "https://d-peru.novopayment.net/core/accounts/v1/debitcard/{tagpay}/chgblockstatus";//"https://d-peru.novopayment.net/core/accounts/v1/debitcard/{tagpay}/chgblockstatus"; prod https://peru.novopayment.net/core/accounts/v1/debitcard/{tagpay}/chgblockstatus";
	private final static String PATH_ASSIGN_CARD_TO_USER = "https://peru.novopayment.net/core/accounts/v1/account";//"https://d-peru.novopayment.net/core/accounts/v1/account"; prod "https://peru.novopayment.net/core/accounts/v1/account";
	private final static String CREDENTIALS  = "grant_type=client_credentials&client_id=96aaf20340eb2386a984709ae5ec5d28&client_secret=2e11a19912ff57fc5f81318973aa5d3a";
	
	
	//QA
	/*private final static String PATH_GET_TOKEN = "https://d-oa.novopayment.net/oauth2/1.0/token"; // pro "; 
	private final static String PATH_REFRESH_TOKEN =  "https://d-oa.novopayment.net/oauth2/1.0/refresh";  
	private final static String PATH_USER_REGISTER = "https://d-peru.novopayment.net/core/enrolls/v1/user"; // prod  "";
	private final static String PATH_CREATE_PREPAID_ACCOUNT = "https://d-peru.novopayment.net/core/accounts/v1/debitcard";   // prod  "";				 
	private final static String PATH_TRANSACTIONS_HISTORY = "https://d-peru.novopayment.net/core/transactionshistory/v1/user/{tagpay}/debitcard/{subbin}/transactions/{limit}";// prob   "";
	private final static String PATH_CHECK_BALANCE = "https://d-peru.novopayment.net/core/accounts/v1/debitcard/user/{tagpay}/balance/{issuerBin}"; //prod  "";// 	
																
	
	private final static String PATH_SEND_MONEY = "https://d-peru.novopayment.net/core/money/v1/accounts/{type}/{requestId}/{issuerBin}" ;
																		
	private final static String PATH_BLOCK_UNBLOCK_ACCOUNT = "https://d-peru.novopayment.net/core/accounts/v1/debitcard/{tagpay}/chgblockstatus";
	private final static String PATH_ASSIGN_CARD_TO_USER = "https://d-peru.novopayment.net/core/accounts/v1/account";
	private final static String CREDENTIALS = "grant_type=client_credentials&client_id=b79dbc9b0af6c2148f80e33c9907d2cf&client_secret=40b9b1f2264b9710a178862bd9fe243b";
	*/
	
	private Gson GSON = new Gson();

	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public TokenRes getTebcaToken() {
		TokenRes response = null;
		try{
			HttpHeaders headers = getHeaders(MediaType.APPLICATION_FORM_URLENCODED_VALUE, "pe", "es");
			
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(PATH_GET_TOKEN);
		               // .queryParam("grant_type", "client_credentials")
		                //.queryParam("client_id", "b79dbc9b0af6c2148f80e33c9907d2cf")
		                //.queryParam("client_secret", "40b9b1f2264b9710a178862bd9fe243b");
			// QA HttpEntity<String> request = new HttpEntity<String>("grant_type=client_credentials&client_id=b79dbc9b0af6c2148f80e33c9907d2cf&client_secret=40b9b1f2264b9710a178862bd9fe243b",headers); 
			// HttpEntity<String> request = new HttpEntity<String>("grant_type=client_credentials&client_id=96aaf20340eb2386a984709ae5ec5d28&client_secret=2e11a19912ff57fc5f81318973aa5d3a",headers);
			HttpEntity<String> request = new HttpEntity<String>(CREDENTIALS,headers);

			LOGGER.debug("ENVIADO PETICION GET TOKEN TEBCA ..");
			ResponseEntity<String> resptebca = restTemplate.exchange(uriBuilder.toUriString(),HttpMethod.POST, request, String.class);
			LOGGER.debug("RESPUESTA DE TEBCA	 - {}", resptebca.getBody());
			response = GSON.fromJson(resptebca.getBody(), TokenRes.class);
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error GET TOKEN " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			if(client.getRawStatusCode() == 400){
				LOGGER.debug("RESPUESTA REGISTRO USUARIO TEBCA {}",client.getResponseBodyAsString());
				/*response = GSON.fromJson(client.getResponseBodyAsString(), response.class);
				try {
					LOGGER.debug(decrypt(token.getAccess_token().substring(5,21),data.getData()));
				} catch (Exception e) {
					LOGGER.debug("ERROR AL RECUPERAR ERROR 400 "+ e);
				}*/
				
			}
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER TOKEN DE ACCESO " + ex.getMessage(), ex);
		}
		
		return response;
	}
	

	@Override
	public TokenRes RefreshToken(String token) {
		TokenRes response = null;
		try{
			
			HttpHeaders headers = getHeaders(MediaType.APPLICATION_FORM_URLENCODED_VALUE, "pe", "es");
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(PATH_REFRESH_TOKEN);
		                //.queryParam("refresh_token", token);
			HttpEntity<String> request = new HttpEntity<String>("refresh_token="+token,headers);
			LOGGER.debug("ENVIADO PETICION REFRESH TOKEN TEBCA .." + uriBuilder.toUriString());
			ResponseEntity<String> resptebca = restTemplate.exchange(uriBuilder.toUriString(),HttpMethod.POST, request, String.class);
			LOGGER.debug("RESPUESTA DE TEBCA	 - {}", resptebca.getBody());
			response = GSON.fromJson(resptebca.getBody(), TokenRes.class);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER REFRESCAR TOKEN DE ACCESO " + ex.getMessage(), ex);
		}
		
		return response;
	}
	
	
	@Override
	public UserRegisterRes userRegister(UserRegisterReq user) {
		UserRegisterRes response = null;
		TokenRes token = getTebcaToken();
		Data data = new Data();
		try{
			LOGGER.debug("entrando al enrolamiento...");
			if(token != null && token.getAccess_token() != null && !token.getAccess_token().isEmpty()){
				//AESAlgorithm aes = new AESAlgorithm(token.getAccess_token().substring(5,21));
				LOGGER.debug("despues del token ...");
				HttpHeaders headers = new HttpHeaders();
				headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
				headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
				headers.set("country", "Pe");
				headers.set("language", "es");
				headers.set("channel", "mobile");
				headers.set("authorization", "Bearer " + token.getAccess_token());
				
				String dataEncry = cifra(token.getAccess_token(), GSON.toJson(user));
				data.setData(dataEncry);
				
				HttpEntity<Data> request = new HttpEntity<Data>(data,headers);
				LOGGER.debug("ENVIANDO REGISTRO DE USUARIO TEBCA {}", GSON.toJson(user));
				LOGGER.debug("ENVIANDO REGISTRO DE USUARIO TEBCA {}", GSON.toJson(data));
				data = (Data) restTemplate.postForObject(PATH_USER_REGISTER, request, Data.class);
				
				LOGGER.debug("RESPUESTA REGISTRO USUARIO TEBCA {}",GSON.toJson(data));
				
				if(data != null){
					response = GSON.fromJson(decrypt(token.getAccess_token().substring(5,21), data.getData()), UserRegisterRes.class);
					LOGGER.debug("RESPUESTA DESCIFRADA: "+ GSON.toJson(response));
				}else
					LOGGER.debug("RESPUESTA NULL TEBCA...");
				
			}else{
				LOGGER.debug("NO SE PUDO OBTENER EL TOKEN DE ACCESO PARA EL REGISTRO");
			}
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error 400 " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			if(client.getRawStatusCode() == 400){
				LOGGER.debug("RESPUESTA REGISTRO USUARIO TEBCA {}",client.getResponseBodyAsString());
				data = GSON.fromJson(client.getResponseBodyAsString(), Data.class);
				try {
					LOGGER.debug(decrypt(token.getAccess_token().substring(5,21),data.getData()));
					response = GSON.fromJson(decrypt(token.getAccess_token().substring(5,21), data.getData()), UserRegisterRes.class);
				} catch (Exception e) {
					LOGGER.debug("ERROR AL RECUPERAR ERROR 400 "+ e);
				}
				
			}
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL REGISTAR USUARIO EN TABCA...", ex);
		}
		return response;
	}
	
	@Override
	public AccountRes createPrepaidAccount(AccountReq accountreq) {
		// TODO Auto-generated method stub
		AccountRes response = null;
		Data data = new Data();
		TokenRes token = getTebcaToken();
		try{
			
			if(token != null && token.getAccess_token() != null && !token.getAccess_token().isEmpty()){
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
				headers.set("content-type", MediaType.APPLICATION_JSON_VALUE);
				headers.set("country", "Pe");
				headers.set("language", "es");
				headers.set("channel", "mobile");
				headers.set("authorization", "Bearer " + token.getAccess_token());
				
				String dataEncry = cifra(token.getAccess_token(), GSON.toJson(accountreq));
				
				data.setData(dataEncry);
				
				HttpEntity<Data> request = new HttpEntity<Data>(data,headers);
				LOGGER.debug("ENVIANDO REGISTRO DE CUENTA PREPAGO TEBCA {}", GSON.toJson(accountreq));
				data = (Data) restTemplate.postForObject(PATH_CREATE_PREPAID_ACCOUNT, request, Data.class);
				
				if(data != null){
					LOGGER.debug("DATA RESPONSE: " + data.getData());
					response = GSON.fromJson(decrypt(token.getAccess_token().substring(5,21), data.getData()), AccountRes.class);
					LOGGER.debug("RESPUESTA REGISTRO descifrada {}",GSON.toJson(response));
				}else{
					LOGGER.debug("RESPUESTA NULL TEBCA (CREAR CUENTA DIGITAL)...");
				}
				
			}else{
				LOGGER.debug("NO SE PUDO OBTENER EL TOKEN DE ACCESO PARA EL REGISTRO DE CUENTA PREPAGO");
			}
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error 400 " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			if(client.getRawStatusCode() == 400){
				//LOGGER.debug("RESPUESTA REGISTRO USUARIO TEBCA {}",client.getResponseBodyAsString());
				data = GSON.fromJson(client.getResponseBodyAsString(), Data.class);
				try {
					LOGGER.debug(decrypt(token.getAccess_token().substring(5,21),data.getData()));
				} catch (Exception e) {
					LOGGER.debug("ERROR AL RECUPERAR MENSAJE ERROR 400 "+ e);
				}
				
			}
		}
		catch(Exception ex){
			LOGGER.error("ERRROR AL CREAR CUENTA PREPAGO...", ex);
		}
		return response;
	}
	
	@Override
	public HistoryTransactions getUserTransactions(String tagpay, String limit,
			String page, String dateOrDays, String filter) {
		HistoryTransactions response = null;
		Data data  = null;
		TokenRes token = getTebcaToken();
		try{
			
			
			if(token != null && token.getAccess_token() != null && !token.getAccess_token().isEmpty()){
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
				headers.set("content-type", MediaType.APPLICATION_JSON_VALUE);
				headers.set("country", "Pe");
				headers.set("language", "es");
				headers.set("channel", "mobile");
				headers.set("authorization", "Bearer " + token.getAccess_token());//{limit}?page={page}&dateOrDays={dateOrDays}&filter={filter}/{issuerBin}
				
				UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(PATH_TRANSACTIONS_HISTORY.replace("{tagpay}", tagpay).replace("{limit}", limit).replace("{subbin}", "48303957"))
		                .queryParam("page", page)
		                .queryParam("dateOrDays", dateOrDays)
		                .queryParam("filter", filter);
				
				HttpEntity<?> request = new HttpEntity<>(headers);
				LOGGER.debug("ENVIADO PETICION GET TRANSACTION HISTORY TEBCA .." + uriBuilder.toUriString());
				ResponseEntity<String> resptebca = restTemplate.exchange(uriBuilder.toUriString(),HttpMethod.GET, request, String.class);
				LOGGER.debug("RESPUESTA DE TEBCA	 - {}", resptebca.getBody());
				data = GSON.fromJson(resptebca.getBody(), Data.class);
	
				String jsonD = decrypt(token.getAccess_token().substring(5,21), data.getData());
				LOGGER.debug("OBJETO DESCIFRADO: " + jsonD);
				response = GSON.fromJson(jsonD, HistoryTransactions.class);
				
				
			}else{
				LOGGER.debug("NO SE PUDO OBTENER EL TOKEN DE ACCESO PARA EL HISTORIAL DE TRANSACCIONES");
			}
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			//if(client.getRawStatusCode() == 400){
				//LOGGER.debug("RESPUESTA REGISTRO USUARIO TEBCA {}",client.getResponseBodyAsString());
				data = GSON.fromJson(client.getResponseBodyAsString(), Data.class);
				try {
					LOGGER.debug(decrypt(token.getAccess_token().substring(5,21),data.getData()));
				} catch (Exception e) {
					LOGGER.debug("ERROR AL RECUPERAR MENSAJE ERROR TRANSACTION "+ e);
				}
				
			//}
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER HISTORIAL DE TRANSACCIONES...", ex);
			
		}
		return response;
	}
	
	@Override
	public Balance checkBalance(String tagpay) {
		Balance balance = null;
		Data data =  new Data();
		TokenRes token = getTebcaToken();
		try{
			
			
			if(token != null && token.getAccess_token() != null && !token.getAccess_token().isEmpty()){
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
				headers.set("content-type", MediaType.APPLICATION_JSON_VALUE);
				headers.set("country", "Pe");
				headers.set("language", "es");
				headers.set("channel", "mobile");
				headers.set("authorization", "Bearer " + token.getAccess_token());
				
				UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(PATH_CHECK_BALANCE.replace("{tagpay}", tagpay).replace("{issuerBin}", "48303957"));
		             
				HttpEntity<?> request = new HttpEntity<>(headers);
				LOGGER.debug("ENVIADO PETICION GET BALANCE ..");
				ResponseEntity<String> resptebca = restTemplate.exchange(uriBuilder.toUriString(),HttpMethod.GET, request, String.class);
				LOGGER.debug("RESPUESTA DE TEBCA BALANCE - {}", resptebca.getBody());
				
				data = GSON.fromJson(resptebca.getBody(), Data.class);
				String JsonDec = decrypt(token.getAccess_token().substring(5,21), data.getData());
				LOGGER.debug("RESPONSE DESCIFRADO: " + JsonDec);
				balance = GSON.fromJson(JsonDec, Balance.class);
				
			}else{
				LOGGER.debug("NO SE PUDO OBTENER EL TOKEN DE ACCESO PARA CHECAR SALDO");
			}
			
		}catch(HttpClientErrorException client){
			
			if(client.getRawStatusCode() != 500){
				//LOGGER.debug("RESPUESTA REGISTRO USUARIO TEBCA {}",client.getResponseBodyAsString());
				data = GSON.fromJson(client.getResponseBodyAsString(), Data.class);
				try {
					String d = decrypt(token.getAccess_token().substring(5,21),data.getData());
					LOGGER.debug("RESPUESTA BALANCE " + d );
					balance = GSON.fromJson(d, Balance.class);
				} catch (Exception e) {
					LOGGER.debug("ERROR AL RECUPERAR MENSAJE ERROR 400 "+ e);
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL CONSULTAR SALDO...", ex);		
			
		}
		return balance;
	}
	
	@Override
	public SendMoneyRes sendMoney(SendMoneyReq transfer, String requestId) {
		SendMoneyRes response = null;
		TokenRes token = getTebcaToken();
		Data data = new Data();
		try{
			
			if(token != null && token.getAccess_token() != null && !token.getAccess_token().isEmpty()){
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
				headers.set("content-type", MediaType.APPLICATION_JSON_VALUE);
				headers.set("country", "Pe");
				headers.set("language", "es");
				headers.set("channel", "mobile");
				headers.set("authorization", "Bearer " + token.getAccess_token());
				
				String jsonReq = GSON.toJson(transfer);
				
				data.setData(cifra(token.getAccess_token(), jsonReq));
				
				HttpEntity<Data> request = new HttpEntity<Data>(data,headers);
				LOGGER.debug("ENVIANDO DINERO {}", GSON.toJson(request));
				String url = PATH_SEND_MONEY.replace("{requestId}", requestId).replace("{type}", "P2P").replace("{issuerBin}", "48303957");
				LOGGER.debug("URL: " + url);
				data = (Data) restTemplate.postForObject(url, request, Data.class);
				LOGGER.debug("RESPUESTA ENVIO DE DINERO {}",GSON.toJson(data));
				
				String jsonDec = decrypt(token.getAccess_token().substring(5,21), data.getData());
				LOGGER.debug("RESPONSE DESCIFRADO: " + jsonDec);
				response = GSON.fromJson(jsonDec, SendMoneyRes.class);
				
			}else{
				LOGGER.debug("NO SE PUDO OBTENER EL TOKEN DE ACCESO PARA EL ENVIO DE DINERO");
			}
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			LOGGER.debug("RESPUESTA ENVIAR DINERO TEBCA {}",client.getResponseBodyAsString());
				try {
					data = GSON.fromJson(client.getResponseBodyAsString(), Data.class);
					LOGGER.debug(decrypt(token.getAccess_token().substring(5,21),data.getData()));
				} catch (Exception e) {
					LOGGER.debug("ERROR AL RECUPERAR ERROR "+ e);
				}
				
			
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL ENVIAR DINERO...", ex);
		}
		return response;
	}
	
	@Override
	public BaseNovoRes blockUnblockAccount(String tagpay) {
		BaseNovoRes response = null;
		TokenRes token = getTebcaToken();
		Data data = new Data();
		try{
			
			if(token != null && token.getAccess_token() != null && !token.getAccess_token().isEmpty()){
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
				headers.set("content-type", MediaType.APPLICATION_JSON_VALUE);
				headers.set("country", "Pe");
				headers.set("language", "es");
				headers.set("channel", "mobile");
				headers.set("authorization", "Bearer " + token.getAccess_token());
				
				UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(PATH_BLOCK_UNBLOCK_ACCOUNT.replace("{tagpay}", tagpay));
		             
				HttpEntity<?> request = new HttpEntity<>(headers);
				LOGGER.debug("ENVIADO PETICION GET BLOQUEO DESBLOQUEO DE CUENTA .." + uriBuilder.toUriString()+"/48303957");
				ResponseEntity<String> resptebca = restTemplate.exchange(uriBuilder.toUriString()+"/48303957",HttpMethod.PUT, request, String.class);
				LOGGER.debug("RESPUESTA DE TEBCA BLOQUEO DESBLOQUEO DE CUENTA - {}", resptebca.getBody());
				
				data = GSON.fromJson(resptebca.getBody(), Data.class);
				String jsonDec = decrypt(token.getAccess_token().substring(5,21), data.getData());
				LOGGER.debug("RESPUESTA DESCIFRADA " + jsonDec);
				
				response = GSON.fromJson(jsonDec, BaseNovoRes.class);
				
			}else{
				LOGGER.debug("NO SE PUDO OBTENER EL TOKEN DE ACCESO PARA BLOQUEAR DESBLOQUEAR CUENTA");
			}
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error  "+ client.getStatusCode() + " "+ client.getStatusText() + "  " + client.getResponseBodyAsString());
			if(client.getRawStatusCode() != 500){
				//LOGGER.debug("RESPUESTA REGISTRO USUARIO TEBCA {}",client.getResponseBodyAsString());
				data = GSON.fromJson(client.getResponseBodyAsString(), Data.class);
				try {
					LOGGER.debug(decrypt(token.getAccess_token().substring(5,21),data.getData()));
					response =  GSON.fromJson(decrypt(token.getAccess_token().substring(5,21),data.getData()), BaseNovoRes.class);
				} catch (Exception e) {
					LOGGER.debug("ERROR AL RECUPERAR MENSAJE ERROR "+ e);
				}
				
			}
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL CONSULTAR BLOQUEAR CUENTA...", ex);
			
		}
		return response;
	}
	
	@Override
	public BaseNovoRes assignCardToUser(AssignCardReq card) {
		BaseNovoRes response = null;
		Data data = new Data();
		TokenRes token = getTebcaToken();
		try{
			
			if(token != null && token.getAccess_token() != null && !token.getAccess_token().isEmpty()){
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
				headers.set("content-type", MediaType.APPLICATION_JSON_VALUE);
				headers.set("country", "Pe");
				headers.set("language", "es");
				headers.set("channel", "mobile");
				headers.set("authorization", "Bearer " + token.getAccess_token());
				
				UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(PATH_ASSIGN_CARD_TO_USER);
		        
				
				data.setData(cifra(token.getAccess_token(), GSON.toJson(card)));
				
				HttpEntity<Data> request = new HttpEntity<Data>(data,headers);
				LOGGER.debug("BODY: " + GSON.toJson(card));
				LOGGER.debug("ENVIADO PETICION ASIGNACION DE CUENTA/TARJETA .." + GSON.toJson(request));
				ResponseEntity<String> resptebca = restTemplate.exchange(uriBuilder.toUriString(),HttpMethod.PUT, request, String.class);
				LOGGER.debug("RESPUESTA DE ASIGNACION DE CUENTA/TARJETA - {}", resptebca.getBody());
				
				data = GSON.fromJson(resptebca.getBody(), Data.class);
				String jsonDes = decrypt(token.getAccess_token().substring(5,21), data.getData());
				LOGGER.debug("RESPONSE DESCIFRADO: " + jsonDes);
				response = GSON.fromJson(jsonDes, BaseNovoRes.class);
				
			}else{
				LOGGER.debug("NO SE PUDO OBTENER EL TOKEN DE ACCESO ASIGNACION DE CUENTA/TARJETA");
			}
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error  "+ client.getStatusCode() + " "+ client.getStatusText() + "  " + client.getResponseBodyAsString());
			if(client.getRawStatusCode() == 400){
				//LOGGER.debug("RESPUESTA REGISTRO USUARIO TEBCA {}",client.getResponseBodyAsString());
				data = GSON.fromJson(client.getResponseBodyAsString(), Data.class);
				try {
					LOGGER.debug(decrypt(token.getAccess_token().substring(5,21),data.getData()));
					String jsonDes = decrypt(token.getAccess_token().substring(5,21), data.getData());
					LOGGER.debug("RESPONSE DESCIFRADO: " + jsonDes);
					response = GSON.fromJson(jsonDes, BaseNovoRes.class);
				} catch (Exception e) {
					LOGGER.debug("ERROR AL RECUPERAR MENSAJE ERROR 400 "+ e);
				}
				
			}
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL ASIGNAR CUENTA/TARJETA...", ex);
			
		}
		return response;
	}
	
	
	private HttpHeaders getHeaders(String contentType, String country, String idioma){
		HttpHeaders headers = new HttpHeaders();
		headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Content-Type", contentType);
		headers.set("x-country", country);
		headers.set("x-language", idioma);
		headers.set("Channel", "Mobile/Web");
		return headers;
	}
	
	
	private String cifra(String token, String texto) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		
		try{ 
		SecretKeySpec skeySpec = new SecretKeySpec(token.substring(5,21).getBytes(), "AES");
		Cipher aesCipherForEncryption = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		
		aesCipherForEncryption.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec("A1CE7E24-14F4-01".getBytes()));
		
		byte[] byteDataToEncrypt = texto.getBytes("UTF-8");
		byte[] byteCipherText = aesCipherForEncryption.doFinal(byteDataToEncrypt);
		String strCipherText = java.util.Base64.getEncoder().encodeToString(byteCipherText);//Base64.encodeBase64String(byteCipherText); 
		return strCipherText;
		}catch(Exception ex){
			LOGGER.error("Error al cifrar datos...", ex);
		}
		return null;
		//Cipher aescf=Cipher.getInstance("AES/CBC/PKCS5Padding");
	   
	}
	
	
	private String decrypt(String key, String encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec("A1CE7E24-14F4-01".getBytes());
        byte[] enc = java.util.Base64.getDecoder().decode(encrypted); //Base64.decodeBase64(encrypted);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
        byte[] decrypted = cipher.doFinal(enc);
        return new String(decrypted);
}
	
	
public void GettebcaCard(long idUsuario, String pan,String vigencia, String codigo,int idApp,int idPais, String idioma,String tipoUsuario){
		
		try{
			
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "application/x-www-form-urlencoded");
			headers.add("Accept", "*/*");
			
			//headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("idUsuario", idUsuario+"");
			map.add("pan", pan);
			map.add("vigencia", vigencia);
			map.add("codigo", codigo);
			
			Map<String, String> params = new HashMap<String, String>();
			params.put("idUsuario", idUsuario+"");
			params.put("pan", pan);
			params.put("vigencia", vigencia);
			params.put("codigo", codigo);

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			LOGGER.debug("ENVIANDO PETICION LOCAL TEBCA ..." + "http://192.168.75.53/Tebca/2/1/es/USUARIO/associateCardMC");
			
			ResponseEntity<String> response = restTemplate.exchange("http://192.168.75.53/Tebca/2/1/es/USUARIO/associateCardMC",HttpMethod.POST, request , String.class );
			//ResponseEntity<String> response = restTemplate.exchange(uriBuilder.toUriString(),HttpMethod.POST, request, String.class);
			
			//String resp = gson.fromJson(response.getBody(), EnrollCardRes.class);
			
			LOGGER.debug("RESPUESTA LOCAL TEBCA... " + response.getBody());
		}catch(Exception ex){
			LOGGER.error("ERROR AL ASOCIAR TARJETA TEBCA", ex);
		}
		return ;
		
	}


}
