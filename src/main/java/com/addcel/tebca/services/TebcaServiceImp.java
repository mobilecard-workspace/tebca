package com.addcel.tebca.services;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.catalina.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.omg.PortableInterceptor.LOCATION_FORWARD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.tebca.client.model.AccountReq;
import com.addcel.tebca.client.model.AccountRes;
import com.addcel.tebca.client.model.AssignCardReq;
import com.addcel.tebca.client.model.Balance;
import com.addcel.tebca.client.model.BaseNovoRes;
import com.addcel.tebca.client.model.HistoryTransactions;
import com.addcel.tebca.client.model.SendMoneyReq;
import com.addcel.tebca.client.model.TokenRes;
import com.addcel.tebca.client.model.UserRegisterReq;
import com.addcel.tebca.client.model.UserRegisterRes;
import com.addcel.tebca.controller.model.BalanceRes;
import com.addcel.tebca.controller.model.BaseResponse;
import com.addcel.tebca.controller.model.EnrollCardRes;
import com.addcel.tebca.controller.model.TransEnroll;
import com.addcel.tebca.controller.model.TransferReq;
import com.addcel.tebca.controller.model.TransferRes;
import com.addcel.tebca.entities.BaseResponseDB;
import com.addcel.tebca.entities.Card;
import com.addcel.tebca.entities.Comercio;
import com.addcel.tebca.entities.TEMPLATE_EMAIL;
import com.addcel.tebca.entities.Transaction;
import com.addcel.tebca.entities.Usuario;
import com.addcel.tebca.repository.DBRepository;
import com.addcel.tebca.repository.Template_Email;
import com.addcel.tebca.repository.UserRepository;
import com.addcel.utils.AddcelCrypto;
import com.fasterxml.jackson.databind.deser.Deserializers.Base;
import com.google.gson.Gson;
import com.sun.javafx.binding.SelectBinding.AsInteger;
import com.sun.org.apache.bcel.internal.generic.LLOAD;

@Service
public class TebcaServiceImp implements TebcaService {

	private static Logger logger = LogManager.getLogger(TebcaServiceImp.class);
			
	@Autowired
	DBRepository repository;
	
	@Autowired
	Template_Email templateEmail;
	
	@Autowired
	UserRepository userRepo;
	@Autowired
	TebcaClientService tebcaService;
	
	private Gson GSON = new Gson();

	@Override
	public TokenRes getToken() {
	
		
		//repository.login("rquiroz", "", "", "es", 1);
		Usuario userOb = GSON.fromJson(repository.transactionTebca(8923233104107l, 1, 1, "es","","","", 1),Usuario.class);//userRepo.getUser(8923233104175l, 1);
		//logger.debug("antes del cifrado...");
		//logger.debug("cifrado tarjeta: " + UtilsService.setSMS("1234567890"));
		//logger.debug("CIFRADO ----->" + AddcelCrypto.encryptHard("1234556"));
		//repository.transactionTebca(8923233104107l, 1, 4, "es", UtilsService.setSMS("123456789"), UtilsService.setSMS("12/20"),UtilsService.setSMS("cvv"), 3);
		//Comercio co = userRepo.g
		//logger.debug(" JSON comercio " + GSON.toJson(userOb));
	
		//Usuario userOb = userRepo.findOne(8923233104175l);
		
		
	 //String resDB = repository.transactionTebca(221, 1, 1, "es","","", "",2); //obtener datos de comercio
		
		
	  //Comercio userObr = GSON.fromJson(resDB, Comercio.class);
	  //logger.debug("Con acentos : " + GSON.toJson(userObr));
		
		
		logger.debug(" JSON OBJECT " + GSON.toJson(userOb));
		//tebcaService.GettebcaCard(8923233104182l, "asdsad", "asdasd", "asdasd", 2, 1, "es", "USUARIO");
		//TokenRes  token = tebcaService.getTebcaToken();
		
		/*String json ="{"    
				+"\"tagpay\":\"44797929\","
				+"\"documentId\":\"44797929\","
				+"\"documentTypeId\":\"1\","
				+"\"firstName\":\"Karen\","
				+"\"middleName\":\"Cassandra\","
				+"\"lastName\":\"Maldonado\","
				+"\"surName\":\"Rocha\","
				+"\"checkDigit\":\"8\","
				+"\"birthday\":\"16/07/1987\","
				+"\"nationality\":\"PE\","
				+"\"countryResidence\":\"PE\","
				+"\"gender\":\"F\","
				+"\"address\":\"Jiron 2, N 675 dep 704, Urb. Monterrico Norte - San Borja\","
				+"\"regionId\":\"15000\","
				+"\"locationId\":\"15008\","
				+"\"subLocationId\":\"8315\","
				+"\"phoneNumber\":\"986664690\","
				+"\"email\":\"kcmaldonado.r@hotmail.com\","
				+"\"workCenter\":\"Gerencia\","
				+"\"occupation\":\"Gerente\","
				+"\"civilServant\":\"0\","
				+"\"publicOffice\":\"\","
				+"\"publicInstitution\":\"\","
				+"\"agreementAccept\":\"1\","
				+"\"acceptProtectData\":\"1\""
				+"}";
		logger.debug("BODY: " + json);
		UserRegisterReq user = GSON.fromJson(json, UserRegisterReq.class);
		tebcaService.userRegister(user);*/
		
		/*************CREANDO CUENTA PREPAGO******************/
	   /*	AccountReq account = new AccountReq();
		account.setIssuerBin("48303957"); 
		account.setIssuerId("16");
		account.setTagpay("08845622");
		tebcaService.createPrepaidAccount(account);*/
		
		
		/**********movimeintos de usuario************/
		//tebcaService.getUserTransactions("44797929", "10", "1", "30", "0");
		
		
		/**********************CHECK BALANCE**********************/
		//tebcaService.checkBalance("44797929");
		
		/*********************enviar dinero******************************/
		/*SendMoneyReq req = new SendMoneyReq();
		req.setAmount("1.00");
		req.setDescription("ENVIO DE PRUEBA");
		req.setTagpayReceive("10526062");
		req.setTagpaySend("12345671");
		req.setCommision("1.00");
		req.setCalculatedComission("1.00");
		
		tebcaService.sendMoney(req, "111101"); // debe contener 6 digitos
		*/
		//tebcaService.checkBalance("12345671");
		//tebcaService.checkBalance("10526062");
		//SendAgreement("ruben.quiroz@mobilecardcorp.com", "Ruben");
		
		/****************************BLOQUEAR/DESBLOQUEAR CUENTA************************************/
	    //tebcaService.blockUnblockAccount("44797929");
	    //tebcaService.blockUnblockAccount("44797929");
		
		/*************************asignar cuenta a usuario*********************************/
		
		/*AssignCardReq card = new AssignCardReq();
		card.setAction("1"); //1 = afiliacion (se enrola el usuario y luego esta opcion para agregar su tarjeta fisica)    2=reposicion    para afiliar tarjeta fisiaca a un usaurio debe oir codigo 1
		card.setTagpay("44797929");
		card.setExpiryDate("0722");
		card.setPan("4830395700001318");
		tebcaService.assignCardToUser(card);*/
		
		/*try {
			token.setMensaje(tebcaService.cifra(token.getAccess_token(), json));
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		
		return new TokenRes();
	}
	
	@Override
	public EnrollCardRes assignCardMC(long idUsuario, int idApp, int idPais, String idioma,  String tipoUsuario){
		EnrollCardRes response = new EnrollCardRes();
		response.setIdError(0);
		response.setMensajeError("");
		TransEnroll register = null;
		
		try{
			logger.debug("INICIANDO PROCESO DE OBTENER CUENTA DIGITAL {} {}", idUsuario, tipoUsuario);
			/********REGISTRANDO USUARIO EN TEBCA****************/
			if(tipoUsuario.toUpperCase().equals("USUARIO"))
				register = enrollUserTebca(idUsuario,idApp,idPais,idioma);
			else if(tipoUsuario.toUpperCase().equals("COMERCIO"))
				register = enrollCommerceTebca(idUsuario, idApp, idPais, idioma);
			
			if(register != null && (register.getRc().equals("0") || register.getRc().equals("-2024")) ){
				//exito
				/******************CREAR TARJETA**********************/
				AccountRes card = createCard(register.getTaypay());
				
				if(card != null && card.getRc().equals("0")){
					//exito
					response.setPan(card.getResponseCard().getPan());
					response.setVigencia(card.getResponseCard().getExpiryDate());
					logger.debug("GUARDANDO TARJETA EN BASE DE DATOS");
					repository.transactionTebca(idUsuario, idApp, idPais, idioma, UtilsService.setSMS(AddcelCrypto.decryptHard(card.getResponseCard().getPan())), UtilsService.setSMS(AddcelCrypto.decryptHard(card.getResponseCard().getExpiryDate())), UtilsService.setSMS(AddcelCrypto.decryptHard("000")), 3);
					//enviando contrato
					SendAgreement(register.getEmail(),register.getNombre());
					
				}else{
					response.setIdError(2000);
					response.setMensajeError( card != null ? card.getMsg() : "ERROR AL ASIGNAR TARJETA, CONTACTE A SOPORTE");
				}
			}else{
				response.setIdError(1000);
				response.setMensajeError(register.getMsg());
			}
			
		}catch(Exception ex){
			logger.error("ERROR AL CREAR TARJETA MOBILECARD {}",idUsuario);
			response.setIdError(3000);
			response.setMensajeError("ERROR AL EMITIR TARJETA MOBILECARD, CONTACTE A SOPORTE");
		}
		
		return response;
	}
	
	@Override
	public EnrollCardRes associateCardMC(long idUsuario, int idApp, int idPais, String idioma, String pan, String expiryDate, String codigo, String tipoUsuario) {
		EnrollCardRes response = new EnrollCardRes();
		TransEnroll  register = null;
		try{
			/********REGISTRANDO USUARIO EN TEBCA****************/
			logger.debug("INICIANDO PROCESO DE ENROLAMIENTO Y ASIGNACION DE TARJETA ...");
			
			if(tipoUsuario.toUpperCase().equals("COMERCIO"))
				register = enrollCommerceTebca(idUsuario, idApp, idPais, idioma);
			else if(tipoUsuario.toUpperCase().equals("USUARIO"))
				register = enrollUserTebca(idUsuario,idApp,idPais,idioma);
			
			if(register != null && (register.getRc().equals("0") || register.getRc().equals("-2024") )){  //| CUANDO YA ESTA REGOSTRADO
				logger.debug("USUARIO REGISTRADO EN TEBCA... SE PROCEDE A ASIGNARLE TARJETA FISICA");
				//exito
				/******************ASIGNAR TARJETA A COMERCIO**********************/
				BaseNovoRes resAssig = associateCard(register.getTaypay(), AddcelCrypto.decryptHard(expiryDate).replace("/", ""), AddcelCrypto.decryptHard(pan),"1");
				if(resAssig != null && resAssig.getRc().equals("0")){
					//exito
					/******************GUARDAR DATOS DE TARJETA**********************/
					response.setIdError(0);
					response.setMensajeError("");
					response.setPan(pan);
					response.setVigencia(expiryDate);
					logger.debug("GUARDANDO TARJETA EN BASE DE DATOS");
					repository.transactionTebca(idUsuario, idApp, idPais, idioma, UtilsService.setSMS(AddcelCrypto.decryptHard(pan)), UtilsService.setSMS(AddcelCrypto.decryptHard(expiryDate)), UtilsService.setSMS(AddcelCrypto.decryptHard(codigo)), 3);
					//enviando contrato
					SendAgreement(register.getEmail(),register.getNombre());
					
				}else{
					logger.debug("ERROR AL ASIGNAR TARJETA A USUARIO: " + GSON.toJson(resAssig));
					response.setIdError(500);
					response.setMensajeError(resAssig != null ? resAssig.getMsg() : "ERROR AL ASIGNAR CUENTA, CONTACTE A SOPORTE");
				}
				
			}else{
				logger.debug("NO SE PUDO REGISTAR USUARIO EN TEBCA: "+GSON.toJson(register));
				response.setIdError(550);
				response.setMensajeError(register != null ? register.getMsg() : "ERROR AL ASIGNAR CUENTA, CONTACTE A SOPORTE");
			}
			
		}catch(Exception ex){
			logger.error("ERROR AL ASIGNAR TARJETA", ex);
			response.setIdError(650);
			response.setMensajeError(register != null ? register.getMsg() : "ERROR AL ASIGNAR CUENTA, CONTACTE A SOPORTE");
			
		}
		logger.debug("retornando respuesta :" + GSON.toJson(response));
		return response;
	}
	
	@Override
	public BalanceRes getBalance(long idUsuario, int idApp, int idPais,
			String idioma, String tipoUsuario) {
	
		BalanceRes balance = null;
		try{
			
			if(tipoUsuario.toUpperCase().equals("USUARIO")){
					Usuario userOb = (Usuario)getDataUserCommerce(idUsuario,tipoUsuario,idApp,idPais,idioma);
					if(userOb != null){
						balance = GSON.fromJson(GSON.toJson(tebcaService.checkBalance(userOb.getCedula())),BalanceRes.class);
					}
				
			}else if(tipoUsuario.toUpperCase().equals("COMERCIO")){
					Comercio comercio = (Comercio)getDataUserCommerce(idUsuario,tipoUsuario,idApp,idPais,idioma);
					if(comercio != null){
						balance = GSON.fromJson(GSON.toJson(tebcaService.checkBalance(comercio.getNumero_documento())),BalanceRes.class); 
					}
			}
			
			if( (balance != null && balance.getRc().equals("-2012"))  || (balance != null && !balance.getRc().equals("0")))
				balance.setActive(0);
			else
				balance.setActive(1);
			
		}catch(Exception ex){
			logger.error("ERROR AL  OBTENER BALANCE " + idUsuario + " " +  tipoUsuario, ex);
		}
		
		return balance;
	}
	
	
	@Override
	public BaseResponse accountLockStatus(long idUsuario, int idApp, int idPais,
			String idioma, String tipoUsuario,int proceso) {
		BaseResponse response = new BaseResponse();
		response.setMensajeError("Error al " +( proceso == 0 ? "bloquear " : "desbloquear " )+ "cuenta" );
		response.setIdError(400);
		String db = ""; 
		try{
			
			if(tipoUsuario.toUpperCase().equals("USUARIO")){
					Usuario userOb = (Usuario)getDataUserCommerce(idUsuario,tipoUsuario,idApp,idPais,idioma);
					if(userOb != null){
						logger.debug("INICIANDO PROCESO DE BLOEQUEO/DESBLOQUE DE CUENTA {} {}", idUsuario,tipoUsuario);
						BaseNovoRes resp = tebcaService.blockUnblockAccount(userOb.getCedula());
						if(resp != null && resp.getRc().equals("0")){
							//exito
							if(proceso == 0)
							   db = repository.transactionTebca(idUsuario, idApp, idPais, idioma, "", "", "", 4);//bloquear tarjeta tebca
							else
								db = repository.transactionTebca(idUsuario, idApp, idPais, idioma, "", "", "", 5);//desbloquear tarjeta tebca
							BaseResponseDB baseError = GSON.fromJson(db, BaseResponseDB.class);
							logger.debug("BLOQUEO EN BASE LOCAL "+ db);
							response.setIdError(0);
							response.setMensajeError("Transacción exitosa");
							
						}else{
							response.setIdError(800);
							response.setMensajeError(resp!=null ? resp.getMsg() : "Error al procesar petición");
						}
							
					}
			}else if(tipoUsuario.toUpperCase().equals("COMERCIO")){
				    logger.debug("INICIANDO PROCESO DE BLOEQUEO/DESBLOQUE DE CUENTA {} {}", idUsuario,tipoUsuario);
				    Comercio comercio = (Comercio)getDataUserCommerce(idUsuario,tipoUsuario,idApp,idPais,idioma);
					if(comercio != null){
						Card card = getCardUserCommerce(idUsuario, tipoUsuario, idApp, idPais, idioma);
						if(card != null){
							BaseNovoRes resp = tebcaService.blockUnblockAccount(comercio.getNumero_documento());
							if(resp != null && resp.getRc().equals("0")){
								//exito
								if(proceso == 0)
								   db = repository.transactionTebca(idUsuario, idApp, idPais, idioma, "", "", "", 4);//bloquear tarjeta tebca
								else
									db = repository.transactionTebca(idUsuario, idApp, idPais, idioma, "", "", "", 5);//desbloquear tarjeta tebca
								BaseResponseDB baseError = GSON.fromJson(db, BaseResponseDB.class);
								logger.debug("BLOQUEO EN BASE LOCAL "+ db);
								response.setIdError(0);
								response.setMensajeError("Transacción exitosa");
								
							}else{
								response.setIdError(800);
								response.setMensajeError(resp!=null ? resp.getMsg() : "Error al procesar petición");
							}
						}else{
							response.setIdError(900);
							response.setMensajeError("El comercio no tiene tarjeta Mobilecard");
						}
						
					}
			}
			
		}catch(Exception ex){
			logger.error("ERROR AL  BLOQUEAR/DESBLOQUEAR CUENTA " + idUsuario + " " +  tipoUsuario, ex);
			response.setIdError(500);
			response.setMensajeError("Error al " +( proceso == 0 ? "bloquear " : "desbloquear " )+ "cuenta" );
		}
		
		return response;
	}
	
	@Override
	public HistoryTransactions getMovements(long idUsuario, int idApp,
			int idPais, String idioma, String tipoUsuario, String limit) {
		HistoryTransactions history = new HistoryTransactions();
		try{
				if(tipoUsuario.toUpperCase().equals("USUARIO")){
					Usuario userOb = (Usuario)getDataUserCommerce(idUsuario,tipoUsuario,idApp,idPais,idioma);
					if(userOb != null){
						history = tebcaService.getUserTransactions(userOb.getCedula(), limit, "1", "30", "0");
					}
				}else if(tipoUsuario.toUpperCase().equals("COMERCIO")){
					Comercio comercio = (Comercio)getDataUserCommerce(idUsuario,tipoUsuario,idApp,idPais,idioma);
					if(comercio != null){
						history = tebcaService.getUserTransactions(comercio.getTipo_documento(),limit, "1", "30", "0");
					}
				}
		}catch(Exception ex){
			logger.error("ERROR AL OBTENER TRANSACCIONES " + tipoUsuario + " " + idUsuario, ex);
		}
		return history;
	}
	
	@Override
	public BaseResponse CardReplacement(long idUsuario, int idApp,int idPais, String idioma, String tipoUsuario){
		    BaseResponse response =  new BaseResponse();
		try{
			
			if(tipoUsuario.toUpperCase().equals("USUARIO")){
				Usuario userOb = (Usuario)getDataUserCommerce(idUsuario,tipoUsuario,idApp,idPais,idioma);
				if(userOb != null && userOb.getId_usuario() != 0){
					Card card = getCardUserCommerce(idUsuario, tipoUsuario, idApp, idPais, idioma);
					if(card != null){
						logger.debug("INICIANDO PROCESO DE REPOSICION {} {} " ,idUsuario, tipoUsuario);
						BaseNovoRes resp = associateCard(userOb.getCedula(), UtilsService.getSMS(card.getVigencia()), UtilsService.getSMS(card.getPan()), "2"); // reposicion
						if(resp != null && resp.getRc().equals("0")){
							String db = repository.transactionTebca(idUsuario, idApp, idPais, idioma, "", "", "", 6);
							logger.debug("RESPUESTA DE BASE REPOSICION " + db);
							response.setIdError(0);
							response.setMensajeError("Transacción exitosa");
						}else{
							response.setIdError(400);
							response.setMensajeError(resp != null ?  resp.getMsg() : "Error en transacción, contacte a soporte");
						}
						
					}else{
						response.setIdError(300);
						response.setMensajeError("El usuario no tiene tarjeta Mobilecard");
					}
				}else{
					logger.debug("NO SE ENCONTRO AL USUARIO");
					response.setIdError(200);
					response.setMensajeError("Usuario no registrado en Mobilecard");
				}
			}else if(tipoUsuario.toUpperCase().equals("COMERCIO")){
				
				Comercio comercio = (Comercio)getDataUserCommerce(idUsuario,tipoUsuario,idApp,idPais,idioma);
				if(comercio != null && comercio.getId() != 0){
					Card card = getCardUserCommerce(idUsuario, tipoUsuario, idApp, idPais, idioma);
					if(card != null){
						logger.debug("INICIANDO PROCESO DE REPOSICION {} {} " ,idUsuario, tipoUsuario);
						BaseNovoRes resp = associateCard(comercio.getNumero_documento(), UtilsService.getSMS(card.getVigencia()), UtilsService.getSMS(card.getPan()), "2"); // reposicion
						if(resp != null && resp.getRc().equals("0")){
							String db = repository.transactionTebca(idUsuario, idApp, idPais, idioma, "", "", "", 6);
							logger.debug("RESPUESTA DE BASE REPOSICION " + db);
							response.setIdError(0);
							response.setMensajeError("Transacción exitosa");
						}else{
							response.setIdError(400);
							response.setMensajeError(resp != null ?  resp.getMsg() : "Error en transacción, contacte a soporte");
						}
					}else{
						response.setIdError(300);
						response.setMensajeError("El comercio no tiene tarjeta Mobilecard");
					}
				}
			}
			
			
		}catch(Exception ex){
			response.setIdError(1000);
			response.setMensajeError("Error en transación, no se pudo realizar la reposición");
		}
		
		return response;
	}
	
	
	@Override
	public TransferRes fundsTransfers(TransferReq req, int idApp, int idPais,
			String idioma) {
		TransferRes response = new TransferRes();
		try{
			logger.debug("INICIANDO PROCESO DE TRANFERENCIA TEBCA tipoUsuarioSend{} {}   tipoUsuarioReceive {} {}", req.getTypeUserSend(),req.getIdUserSend(), req.getTypeUserReceive(), req.getIdUserReceive());
			
		}catch(Exception ex){
			
		}
		
		return response;
	}
	
	private Card getCardUserCommerce(long idUsuario,String tipoUsuario, int idApp, int idPais, String idioma){
		try{
			if(tipoUsuario.toUpperCase().equals("USUARIO")){
				String resDB = repository.transactionTebca(idUsuario, idApp, idPais, idioma,"","","",1);// obtener datos de usuario
				BaseResponseDB baseError = GSON.fromJson(resDB,BaseResponseDB.class);
				if(baseError != null && baseError.getIdError() == 0)
					return GSON.fromJson(resDB, Card.class);
			}else if(tipoUsuario.toUpperCase().equals("COMERCIO")){
				String resDB = repository.transactionTebca(idUsuario, idApp, idPais, idioma,"","","", 2); //obtener datos de comercio
				BaseResponseDB baseError = GSON.fromJson(resDB,BaseResponseDB.class);
				if(baseError != null  && baseError.getIdError() == 0)
					return GSON.fromJson(resDB, Card.class);
			}
		}catch(Exception ex){
			logger.error("ERROR ", ex);
			logger.error("ERROR AL OBTENER DATOS DE TARJETA {} {}",idUsuario, tipoUsuario);
		}
		return null;
	}
	
	private Object getDataUserCommerce(long idUsuario,String tipoUsuario, int idApp, int idPais, String idioma){
		
		try{
			if(tipoUsuario.toUpperCase().equals("USUARIO")){
				String resDB = repository.transactionTebca(idUsuario, idApp, idPais, idioma,"","","",1);// obtener datos de usuario
				BaseResponseDB baseError = GSON.fromJson(resDB,BaseResponseDB.class);
				if(baseError != null && baseError.getIdError() == 0)
					return GSON.fromJson(resDB, Usuario.class);
			}else if(tipoUsuario.toUpperCase().equals("COMERCIO")){
				String resDB = repository.transactionTebca(idUsuario, idApp, idPais, idioma,"","","", 2); //obtener datos de comercio
				BaseResponseDB baseError = GSON.fromJson(resDB,BaseResponseDB.class);
				if(baseError != null  && baseError.getIdError() == 0)
					return GSON.fromJson(resDB, Comercio.class);
			}
		}catch(Exception ex){
			logger.error("ERROR AL MOMENTO DE EXTRAER DATOS " + tipoUsuario + " " + idUsuario, ex);
		}
		return null;
	}
	
	private TransEnroll enrollUserTebca(long idUsuario, int idApp, int idPais, String idioma){
		String protectData ="";
		UserRegisterRes responseTebca = null;
		BaseResponseDB responseDB = null;
		TransEnroll response = null; 
		String resDB = "";
		try{
			logger.debug("iniciando proceso de enrolamiento usuario");
			resDB = repository.transactionTebca(idUsuario, idApp, idPais, idioma,"","","",1);
			logger.debug("respuesta base de datos: " + resDB);
			responseDB = GSON.fromJson(resDB,BaseResponseDB.class);
			if(responseDB != null  && responseDB.getIdError() == 0){
				Usuario userOb = GSON.fromJson(resDB, Usuario.class);//userRepo.getUser(idUsuario, idApp);
				if((userOb.getUsr_cargo_publico() == null && userOb.getUsr_institucion() == null) ||  (userOb.getUsr_cargo_publico().isEmpty() && userOb.getUsr_institucion().isEmpty()) )
					protectData = "0";
				else
					protectData = "1";
				//usr_direccion,usr_fecha_nac,digito_verificador,cedula,tipocedula,eMail,usr_nombre,usr_sexo,usr_apellido,usr_provincia,usr_nacionalidad
				//usr_ocupacion,usr_telefono,usr_institucion,usr_departamento,usr_distrito,usr_departamento,usr_centro_laboral,taypay
				String firstname = userOb.getUsr_nombre().split(" ")[0];
				String MiddleName = userOb.getUsr_nombre().split(" ").length > 1 ? userOb.getUsr_nombre().split(" ")[1] : "";
				
				UserRegisterReq user = new UserRegisterReq();
				user.setAgreementAccept("1");
				user.setAcceptProtectData(protectData);
				user.setAddress(userOb.getUsr_direccion()); //usr_direccion
				user.setBirthday(userOb.getUsr_fecha_nac());// usr_fecha_nac
				user.setCheckDigit(userOb.getDigito_verificador()); // sera campo en t_usuario
				user.setCivilServant("0");
				user.setDocumentId(userOb.getCedula());//cedula
				user.setDocumentTypeId(userOb.getTipocedula());//tipocedula
				user.setEmail(userOb.geteMail()); //eMail
				user.setFirstName(firstname); //usr_nombre
				user.setGender(userOb.getUsr_sexo()); // usr_sexo
				user.setLastName(userOb.getUsr_apellido()); // usr_apellido
				user.setLocationId(userOb.getUsr_provincia()); //usr_provincia
				user.setMiddleName(MiddleName); // este podemos enviarlo vacio
				user.setNationality(userOb.getUsr_nacionalidad()); //usr_nacionalidad
				user.setOccupation(userOb.getUsr_ocupacion()); // usr_ocupacion
				user.setPhoneNumber(userOb.getUsr_telefono()); //usr_telefono
				user.setPublicInstitution(userOb.getUsr_institucion()); //usr_institucion
				user.setPublicOffice(userOb.getUsr_institucion()); // usr_institucion
				user.setRegionId(userOb.getUsr_departamento()); // usr_departamento
				user.setCountryResidence(userOb.getUsr_nacionalidad()); //usr_nacionalidad
				user.setSubLocationId(userOb.getUsr_distrito()); // usr_distrito
				user.setSurName(""); // puede enviarse como vacio el segundo apellido
				user.setTagpay(userOb.getCedula()); // tiene que ser el documentID 8 digitos ya que sino manda error "Error formato parametros"
				user.setWorkCenter(userOb.getUsr_centro_laboral()); // usr_centro_laboral
				
				responseTebca = tebcaService.userRegister(user);
				
				if(responseTebca != null){
					response = new TransEnroll();
					response.setMsg(responseTebca.getMsg());
					response.setRc(responseTebca.getRc());
					response.setTaypay(user.getTagpay());
					response.setTransactionDate(responseTebca.getTransactionDate());
					response.setEmail(userOb.geteMail());
					response.setNombre(userOb.getUsr_nombre());
				}
			}else{
				response = new TransEnroll();
				response.setRc("600");
				response.setMsg("Usuario no registrado en Mobilecard");
			}
			
		}catch(Exception ex){
			logger.error("ERROR AL REGISTRAR USUARIO EN TEBCA ", ex);
		}
		return response;
	}
	
	
	private TransEnroll enrollCommerceTebca(long idComercio, int idApp, int idPais, String idioma){
		String protectData ="";
		UserRegisterRes responseTebca = null;
		BaseResponseDB responseDB = null;
		TransEnroll response = null;
		String resDB = "";
		try{
			logger.debug("INICIANDO ENROLAMIENTO DE COMMERCIO");
			resDB = repository.transactionTebca(idComercio, idApp, idPais, idioma,"","", "",2); //obtener datos de comercio
			responseDB = GSON.fromJson(resDB,BaseResponseDB.class);
			if(responseDB != null  && responseDB.getIdError() == 0){
				Comercio userOb = GSON.fromJson(resDB, Comercio.class);//userRepo.getUser(idUsuario, idApp);
				if((userOb.getCargo() == null && userOb.getInstitucion() == null) ||  (userOb.getCargo().isEmpty() && userOb.getInstitucion().isEmpty()) )
					protectData = "0";
				else
					protectData = "1";
				
				String firstname = userOb.getRepresentante_nombre().split(" ")[0];
				String middlename =  userOb.getRepresentante_nombre().split(" ").length > 1 ?  userOb.getRepresentante_nombre().split(" ")[1] : ""; 
				
				UserRegisterReq user = new UserRegisterReq();
				user.setAgreementAccept("1");
				user.setAcceptProtectData(protectData);
				user.setAddress(userOb.getDireccion_establecimiento()); //usr_direccion
				user.setBirthday(userOb.getFecha_nacimiento());// usr_fecha_nac
				user.setCheckDigit(userOb.getDigito_verificador()); // sera campo en t_usuario
				user.setCivilServant(userOb.getInstitucion()!= null && !userOb.getInstitucion().isEmpty() ? "1" : "0");
				user.setDocumentId(userOb.getNumero_documento());//cedula
				user.setDocumentTypeId("1");//tipocedula userOb.getTipo_documento()
				user.setEmail(userOb.getEmail_contacto()); //eMail
				user.setFirstName(firstname); //usr_nombre
				user.setGender("M"); // usr_sexo
				user.setLastName(userOb.getRepresentante_paterno()); // usr_apellido
				user.setLocationId(userOb.getProvincia()); //usr_provincia
				user.setMiddleName(middlename); // este podemos enviarlo vacio
				user.setNationality("PE"); // userOb.getNacionalidad() usr_nacionalidad
				user.setOccupation(userOb.getOcupacion()); // usr_ocupacion
				user.setPhoneNumber(userOb.getTelefono_contacto()); //usr_telefono
				user.setPublicInstitution(userOb.getInstitucion()); //usr_institucion
				user.setPublicOffice(userOb.getInstitucion()); // usr_institucion
				user.setRegionId(userOb.getDepartamento()); // usr_departamento
				user.setCountryResidence("PE"); // userOb.getNacionalidad() usr_nacionalidad
				user.setSubLocationId(userOb.getDistrito()); // usr_distrito
				user.setSurName(""); // puede enviarse como vacio el segundo apellido
				user.setTagpay(userOb.getNumero_documento()); // que sea idUsuario
				user.setWorkCenter(userOb.getCentro_laboral()); // usr_centro_laboral
				
				responseTebca = tebcaService.userRegister(user);
				
				if(responseTebca != null){
					response = new TransEnroll();
					response.setMsg(responseTebca.getMsg());
					response.setRc(responseTebca.getRc());
					response.setTaypay(user.getTagpay());
					response.setTransactionDate(responseTebca.getTransactionDate());
					response.setEmail(userOb.getEmail_contacto());
					response.setNombre(userOb.getRepresentante_nombre());
				}
			}else{
				response = new TransEnroll();
				response.setRc("600");
				response.setMsg("Comercio no registrado en Mobilecard");
			}
			
		}catch(Exception ex){
			logger.error("ERROR AL REGISTRAR COMERCIO EN TEBCA ", ex);
		}
		return response;
	}
	
	
	private AccountRes createCard(String  tagpay){
		
		AccountRes response = null;
		try{
			logger.debug("PREPARANDO REQUEST PARA CUENTA DIGITAL");
			AccountReq account = new AccountReq();
			account.setIssuerBin("48303957");
			account.setIssuerId("16");
			account.setTagpay(tagpay);
			logger.debug("ENVIADO PETICION CUENTA DIGITAL " + GSON.toJson(account));
			response = tebcaService.createPrepaidAccount(account);
			
		}catch(Exception ex){
			logger.error("ERROR AL CREAR TARJETA...", ex);
		}
		return response;
		
	}
	
private BaseNovoRes associateCard(String documentId, String expiryDate, String pan, String action){
		
	BaseNovoRes response = null;
		try{
			
			AssignCardReq req = new AssignCardReq();
			req.setAction(action);  // 2 = reposicion, 1 = afiliacion (tarjeta fisica) 
			req.setTagpay(documentId);
			req.setExpiryDate(expiryDate);
			req.setPan(pan);
	
			response = tebcaService.assignCardToUser(req);
			
		}catch(Exception ex){
			logger.error("ERROR AL CREAR ASOCIAR TARJETA...", ex);
		}
		return response;
		
	}

private void SendAgreement (String  sMail,String nombre){
	try{ 
		
		 TEMPLATE_EMAIL template = templateEmail.findOne("@MENSAJE_CONTRATO_TEBCA");
		 MailSender correo = new MailSender();
	     String from = "no-reply@addcel.com";
	     String[] to = {sMail};
	     correo.setCc(new String[] {});
	     correo.setBody(template.getCUERPO().replace("@NOMBRE", nombre));
	     correo.setFrom(from);
	     correo.setSubject(template.getASUNTO());
	     correo.setTo(to);
	     correo.setCid(new String[] {  "/usr/java/resources/images/Addcel/user_registry.PNG" });
	     correo.setAttachments(new String[] {"/usr/java/resources/contratos/CONTRATOTEBCA.pdf"});
	     logger.debug("ENVIANDO CONTRATO ");
	     EmailSenderClient client = new EmailSenderClient();
	     client.SendMail(correo);
	}catch(Exception ex){
		logger.error("ERROR AL ENVIAR CONTRATO TEBCA", ex);
	}
}
	
	
}
