package com.addcel.tebca.services;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.addcel.tebca.client.model.AccountReq;
import com.addcel.tebca.client.model.AccountRes;
import com.addcel.tebca.client.model.AssignCardReq;
import com.addcel.tebca.client.model.Balance;
import com.addcel.tebca.client.model.BaseNovoRes;
import com.addcel.tebca.client.model.History;
import com.addcel.tebca.client.model.HistoryTransactions;
import com.addcel.tebca.client.model.JumioVerifyRes;
import com.addcel.tebca.client.model.SendMoneyReq;
import com.addcel.tebca.client.model.SendMoneyRes;
import com.addcel.tebca.client.model.TokenRes;
import com.addcel.tebca.client.model.UserRegisterReq;
import com.addcel.tebca.client.model.UserRegisterRes;



public interface TebcaClientService {
	
	TokenRes getTebcaToken();
	
	TokenRes RefreshToken(String token);
	
	UserRegisterRes userRegister(UserRegisterReq user);
	
	AccountRes createPrepaidAccount(AccountReq accountreq);
	
	HistoryTransactions getUserTransactions(String tagpay, String limit, String page, String dateOrDays, String filter);
	
	Balance checkBalance(String tagpay);
	
	SendMoneyRes sendMoney(SendMoneyReq transfer, String requestId);
	
	BaseNovoRes blockUnblockAccount(String tagpay);
	
	BaseNovoRes assignCardToUser(AssignCardReq card);
	
	void GettebcaCard(long idUsuario, String pan,String vigencia, String codigo,int idApp,int idPais, String idioma,String tipoUsuario);
	
	//String cifra(String token, String texto)throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException;
	
	//String decrypt(String key, String iv, String encrypted) throws Exception;

	/*TokenResponse tokenGenerator(String idioma, String employeeId, String programId, String trxid, TokenRequest tokenRequest);
	
	AuthTokenResponse tokenAuthentication(String idioma,String employeeId, String programId, String trxid, AuthTokenRequest tokenRequest);
	
	RegistrationCardResponse registerCard(String idioma, String programId, String trxid, RegistrationCardRequest cardRequest);
	
	UpdateCardResponse UpdateCard(String idioma, String programId, String trxid, UpdateCardRequest cardRequest);*/
}
