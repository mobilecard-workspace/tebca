package com.addcel.tebca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EntityScan("com.addcel.tebca.entities")
@ComponentScan({"com.addcel.tebca.controller","com.addcel.tebca.services"})
@EnableJpaRepositories(basePackages  = "com.addcel.tebca.repository")
@SpringBootApplication
@EnableSwagger2
public class TebcaApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(TebcaApplication.class, args);
	}

	
	@Bean
	public RestTemplate getRestTemplate() {
	    return new RestTemplate();
	}
}
