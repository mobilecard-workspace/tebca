package com.addcel.tebca.controller.model;

public class TransferReq {

	private long idUserSend;
	private long idUserReceive;
	private String typeUserSend;
	private String typeUserReceive;
	private int idProveedor;
	private int idProducto;
	private String concept;
	private double amount;
	private double comsion;
	private String ticket;
	private String imei;
	private String software;
	private String model;
	private String latitude;
	private String longitude;
	
	public TransferReq() {
		// TODO Auto-generated constructor stub
	}

	public long getIdUserSend() {
		return idUserSend;
	}

	public void setIdUserSend(long idUserSend) {
		this.idUserSend = idUserSend;
	}

	public long getIdUserReceive() {
		return idUserReceive;
	}

	public void setIdUserReceive(long idUserReceive) {
		this.idUserReceive = idUserReceive;
	}

	public String getTypeUserSend() {
		return typeUserSend;
	}

	public void setTypeUserSend(String typeUserSend) {
		this.typeUserSend = typeUserSend;
	}

	public String getTypeUserReceive() {
		return typeUserReceive;
	}

	public void setTypeUserReceive(String typeUserReceive) {
		this.typeUserReceive = typeUserReceive;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getConcept() {
		return concept;
	}

	public void setConcept(String concept) {
		this.concept = concept;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getComsion() {
		return comsion;
	}

	public void setComsion(double comsion) {
		this.comsion = comsion;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
}
