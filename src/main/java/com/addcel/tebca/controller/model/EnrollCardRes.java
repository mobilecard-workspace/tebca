package com.addcel.tebca.controller.model;

public class EnrollCardRes extends BaseResponse{

	private String pan;
	private String vigencia;
	
	public EnrollCardRes() {
		// TODO Auto-generated constructor stub
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	
}
