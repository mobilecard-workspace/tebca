package com.addcel.tebca.controller.model;

import com.addcel.tebca.client.model.UserRegisterRes;

public class TransEnroll extends UserRegisterRes{
	
	private String taypay;
	private String email;
	private String nombre;
	
	public TransEnroll() {
		// TODO Auto-generated constructor stub
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setTaypay(String taypay) {
		this.taypay = taypay;
	}
	
	public String getTaypay() {
		return taypay;
	}
	
}
