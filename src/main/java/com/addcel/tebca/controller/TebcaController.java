package com.addcel.tebca.controller;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.tebca.client.model.TokenRes;
import com.addcel.tebca.controller.model.AssigCardReq;
import com.addcel.tebca.controller.model.BaseResponse;
import com.addcel.tebca.controller.model.TransferReq;
import com.addcel.tebca.services.TebcaService;

@RestController
public class TebcaController {

	private static final String PATH_CREATE_CARDMC = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/assignCardMC";
	
	private static final String PATH_ASSOCIATE_CARDMC = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/associateCardMC";
	
	private static final String PATH_GET_BALANCE = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/balance";
	
	private static final String PATH_ACCCOUNT_LOCK_UNLOCK = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/chgblockstatus";
	
	private static final String PATH_GET_MOVEMENTS = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/movements/{limit}";
	
	private static final String PATH_ACCOUNT_REPLACEMENT = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/replacement";
	
	private static final String PATH_FUNDS_TRANSFER = "{idApp}/{idPais}/{idioma}/fundstransfer";
	
	@Autowired
	TebcaService service;
	

	
	@RequestMapping(value="/getToken", method = RequestMethod.GET)
	@ApiOperation(value = "Peticion de token",
    			notes = "Peticion de token para usarlo en servicios",
    			response = TokenRes.class,
    			responseContainer = "Object")
	public TokenRes gettoken(){
		return service.getToken();
	}
	
	@RequestMapping(value = PATH_CREATE_CARDMC, method = RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Object> assignCardMC(@PathVariable int idApp,@PathVariable int idPais,@PathVariable String idioma,@PathVariable String tipoUsuario,@RequestParam("idUsuario") long idUsuario){
		return new ResponseEntity<Object>(service.assignCardMC(idUsuario, idApp, idPais, idioma, tipoUsuario),HttpStatus.OK);
	}
	
	@RequestMapping(value = PATH_ASSOCIATE_CARDMC, method = RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Object> associateCardMC(@PathVariable int idApp,@PathVariable int idPais,@PathVariable String idioma,@PathVariable String tipoUsuario,
			/*@PathVariable String tipoUsuario,@RequestParam("idUsuario") long idUsuario
												,@RequestParam("pan") String pan,@RequestParam("vigencia") String vigencia,@RequestParam("codigo") String codigo)*/
			@RequestBody AssigCardReq req){
		return new ResponseEntity<Object>(service.associateCardMC(req.getIdUsuario(), idApp, idPais, idioma, req.getPan(), req.getVigencia(), req.getCodigo(),tipoUsuario),HttpStatus.OK);
	}
	
	@RequestMapping(value = PATH_GET_BALANCE, method = RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Object> getBalance(@PathVariable int idApp,@PathVariable int idPais,@PathVariable String idioma,@PathVariable String tipoUsuario,@RequestParam("idUsuario") long idUsuario
			){
		return new ResponseEntity<Object>(service.getBalance(idUsuario, idApp, idPais, idioma, tipoUsuario),HttpStatus.OK);
	}
	
	@RequestMapping(value = PATH_ACCCOUNT_LOCK_UNLOCK, method = RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Object> accountLockStatus(@PathVariable int idApp,@PathVariable int idPais,@PathVariable String idioma,@PathVariable String tipoUsuario,@RequestParam("id") long id
			,@RequestParam("proceso") Integer proceso){
		return new ResponseEntity<Object>(service.accountLockStatus(id, idApp, idPais, idioma, tipoUsuario,proceso),HttpStatus.OK);
	}
	
	@RequestMapping(value = PATH_GET_MOVEMENTS, method = RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Object> getMovements(@PathVariable int idApp,@PathVariable int idPais,@PathVariable String limit,@PathVariable String idioma,@PathVariable String tipoUsuario,@RequestParam("idUsuario") long idUsuario
			){
		
		return new ResponseEntity<Object>(service.getMovements(idUsuario, idApp, idPais, idioma, tipoUsuario, limit),HttpStatus.OK);
	}
	
	@RequestMapping(value = PATH_ACCOUNT_REPLACEMENT, method = RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Object> replacement(@PathVariable int idApp,@PathVariable int idPais,@PathVariable String limit,@PathVariable String idioma,@PathVariable String tipoUsuario
			,@RequestParam("id") long id){
		
		return new ResponseEntity<Object>(service.CardReplacement(id, idApp, idPais, idioma, tipoUsuario),HttpStatus.OK);
	}
	
	@RequestMapping(value = PATH_FUNDS_TRANSFER, method = RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Object> replacement(@PathVariable int idApp,@PathVariable int idPais,@PathVariable String limit,@PathVariable String idioma
			,@RequestBody TransferReq req){
		
		return new ResponseEntity<Object>(service.fundsTransfers(req, idApp, idPais, idioma),HttpStatus.OK);
	}
	
}
