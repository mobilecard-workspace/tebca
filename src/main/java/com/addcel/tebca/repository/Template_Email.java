package com.addcel.tebca.repository;

import org.springframework.data.repository.CrudRepository;

import com.addcel.tebca.entities.TEMPLATE_EMAIL;

public interface Template_Email extends CrudRepository<TEMPLATE_EMAIL,String> {

}
