package com.addcel.tebca.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.tebca.entities.Comercio;
import com.addcel.tebca.entities.Usuario;


public interface UserRepository extends CrudRepository<Usuario,Long> {
	
	@Query(value = "select id_usuario,usr_direccion,usr_cargo_publico,digito_verificador,DATE_FORMAT(usr_fecha_nac, '%d/%m/%Y') as usr_fecha_nac,cedula,tipocedula,eMail,usr_nombre,usr_sexo,usr_apellido,usr_provincia,usr_nacionalidad,usr_ocupacion,usr_telefono,usr_institucion,usr_departamento,usr_distrito,usr_centro_laboral FROM t_usuarios WHERE id_usuario = :idUsuario and id_aplicacion = :idApp", nativeQuery = true)
	Usuario getUser(@Param("idUsuario") long idUsuario, @Param("idApp") int idApp);
	
  /* @Query(value = "select id,direccion_establecimiento, cargo,digito_verificador,DATE_FORMAT(fecha_nacimiento, '%d/%m/%Y') as fecha_nacimiento,numero_documento,tipo_documento,email_contacto, representante_nombre, representante_paterno, representante_materno,provincia,nacionalidad,ocupacion,telefono_contacto,institucion,departamento,distrito,centro_laboral from LCPF_establecimiento where id = :idComercio and id_aplicacion = :idApp", nativeQuery = true)
   Comercio getComercio(@Param("idComercio") long idComercio, @Param("idApp") int idApp);*/
}
