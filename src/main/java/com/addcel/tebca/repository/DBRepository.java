package com.addcel.tebca.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.tebca.entities.Transaction;
import com.addcel.tebca.entities.Usuario;

public interface DBRepository extends CrudRepository<Transaction,Long> {

	@Query(value = "select loginMobilecard(:user,:pass,:idioma,:tipo,:idApp)", nativeQuery = true )
	String login(@Param("user") String user, @Param("pass") String pass,@Param("tipo") String tipo, @Param("idioma") String idioma, @Param("idApp") Integer idApp);
	
	@Query(value = "select business_tebca(:idUsuario,:idApp,:idPais,:idioma,:pan,:vigencia,:codigo,:proceso);", nativeQuery = true)
	String transactionTebca(@Param("idUsuario") long idUsuario, @Param("idApp") int idApp,@Param("idPais") int idPais,@Param("idioma") String idioma,@Param("pan") String pan,@Param("vigencia") String vigencia,@Param("codigo") String codigo,@Param("proceso") int proceso);
	
	//@Query(value = "select estado_tebca from tarjetas_usuario where tebca = 1", nativeQuery = true)
	
}
