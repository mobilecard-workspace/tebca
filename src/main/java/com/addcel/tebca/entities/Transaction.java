package com.addcel.tebca.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transaccion_tebca")
public class Transaction {

    @Id
    @Column(name = "id")
    private long id;
    
    @Column(name = "request")
    private String request;
    
    @Column(name = "response")
    private  String response;
    
    @Column(name = "process")
    private String process;
    
    @Column(name = "tagpay")
    private String tagpay;
    
    @Column(name = "date")
    private Date date;
    
    @Column(name = "idApp")
    private int idApp;
    
    @Column(name = "idCountry")
    private int idCountry;
    
    public Transaction() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getTagpay() {
		return tagpay;
	}

	public void setTagpay(String tagpay) {
		this.tagpay = tagpay;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getIdApp() {
		return idApp;
	}

	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}

	public int getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(int idCountry) {
		this.idCountry = idCountry;
	}
    
}
