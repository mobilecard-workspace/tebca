package com.addcel.tebca.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

@Entity
@Table(name="t_usuarios")
/*@NamedNativeQueries({
	@NamedNativeQuery(
			name	=	"getComercio",
			query	=	"select id,direccion_establecimiento, cargo,digito_verificador,DATE_FORMAT(fecha_nacimiento, '%d/%m/%Y') as fecha_nacimiento,numero_documento,tipo_documento,email_contacto, representante_nombre, representante_paterno, representante_materno,provincia,nacionalidad,ocupacion,telefono_contacto,institucion,departamento,distrito,centro_laboral from LCPF_establecimiento where id = :idComercio and id_aplicacion = :idApp",
						resultClass=Comercio.class
	)
})*/

public class Usuario {

	@Id
    @Column(name = "id_usuario")
	private long id_usuario;
	
	@Column(name = "usr_direccion")
	private String usr_direccion;
	
	@Column(name = "usr_fecha_nac")
	private String usr_fecha_nac;
	
	@Column(name = "cedula")
	private String cedula; 
	
	@Column(name = "tipocedula")
	private String tipocedula;
	
	@Column(name = "eMail")
	private String eMail;
	
	@Column(name = "usr_nombre")
	private String usr_nombre;
	
	@Column(name = "usr_sexo")
	private String usr_sexo;
	
	@Column(name = "usr_apellido")
	private String usr_apellido;
	
	@Column(name = "usr_provincia")
	private String usr_provincia;
	
	@Column(name = "usr_nacionalidad")
	private String usr_nacionalidad;
	
	@Column(name = "usr_ocupacion")
	private String usr_ocupacion;
	
	@Column(name = "usr_telefono")
	private String usr_telefono;
	
	@Column(name = "usr_institucion")
	private String usr_institucion;
	
	@Column(name = "usr_departamento")
	private String usr_departamento;
	
	@Column(name = "usr_distrito")
	private String usr_distrito;
	
	@Column(name = "usr_centro_laboral")
	private String usr_centro_laboral;
	
	@Column(name = "usr_cargo_publico")
	private String usr_cargo_publico;
	
	@Column(name = "digito_verificador")
	private String digito_verificador;
	
	public Usuario() {
		// TODO Auto-generated constructor stub
	}

	public void setDigito_verificador(String digito_verificador) {
		this.digito_verificador = digito_verificador;
	}
	
	public String getDigito_verificador() {
		return digito_verificador;
	}
	
	public String getUsr_cargo_publico() {
		return usr_cargo_publico;
	}
	
	public void setUsr_cargo_publico(String usr_cargo_publico) {
		this.usr_cargo_publico = usr_cargo_publico;
	}
	
	public long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getUsr_direccion() {
		return usr_direccion;
	}

	public void setUsr_direccion(String usr_direccion) {
		this.usr_direccion = usr_direccion;
	}

	public String getUsr_fecha_nac() {
		return usr_fecha_nac;
	}

	public void setUsr_fecha_nac(String usr_fecha_nac) {
		this.usr_fecha_nac = usr_fecha_nac;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getTipocedula() {
		return tipocedula;
	}

	public void setTipocedula(String tipocedula) {
		this.tipocedula = tipocedula;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getUsr_nombre() {
		return usr_nombre;
	}

	public void setUsr_nombre(String usr_nombre) {
		this.usr_nombre = usr_nombre;
	}

	public String getUsr_sexo() {
		return usr_sexo;
	}

	public void setUsr_sexo(String usr_sexo) {
		this.usr_sexo = usr_sexo;
	}

	public String getUsr_apellido() {
		return usr_apellido;
	}

	public void setUsr_apellido(String usr_apellido) {
		this.usr_apellido = usr_apellido;
	}

	public String getUsr_provincia() {
		return usr_provincia;
	}

	public void setUsr_provincia(String usr_provincia) {
		this.usr_provincia = usr_provincia;
	}

	public String getUsr_nacionalidad() {
		return usr_nacionalidad;
	}

	public void setUsr_nacionalidad(String usr_nacionalidad) {
		this.usr_nacionalidad = usr_nacionalidad;
	}

	public String getUsr_ocupacion() {
		return usr_ocupacion;
	}

	public void setUsr_ocupacion(String usr_ocupacion) {
		this.usr_ocupacion = usr_ocupacion;
	}

	public String getUsr_telefono() {
		return usr_telefono;
	}

	public void setUsr_telefono(String usr_telefono) {
		this.usr_telefono = usr_telefono;
	}

	public String getUsr_institucion() {
		return usr_institucion;
	}

	public void setUsr_institucion(String usr_institucion) {
		this.usr_institucion = usr_institucion;
	}

	public String getUsr_departamento() {
		return usr_departamento;
	}

	public void setUsr_departamento(String usr_departamento) {
		this.usr_departamento = usr_departamento;
	}

	public String getUsr_distrito() {
		return usr_distrito;
	}

	public void setUsr_distrito(String usr_distrito) {
		this.usr_distrito = usr_distrito;
	}

	public String getUsr_centro_laboral() {
		return usr_centro_laboral;
	}

	public void setUsr_centro_laboral(String usr_centro_laboral) {
		this.usr_centro_laboral = usr_centro_laboral;
	}
	
}
