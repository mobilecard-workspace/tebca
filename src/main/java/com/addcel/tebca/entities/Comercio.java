package com.addcel.tebca.entities;

public class Comercio {

	private long id;
	private String direccion_establecimiento;
	private String cargo;
	private String digito_verificador;
	private String  fecha_nacimiento;
	private String numero_documento;
	private String tipo_documento;
	private String email_contacto; 
	private String representante_nombre;
	private String representante_paterno;
	private String representante_materno;
	private String provincia;
	private String nacionalidad;
	private String ocupacion;
	private String telefono_contacto;
	private String institucion;
	private String departamento;
	private String distrito;
	private String centro_laboral;
	
	public Comercio() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDireccion_establecimiento() {
		return direccion_establecimiento;
	}

	public void setDireccion_establecimiento(String direccion_establecimiento) {
		this.direccion_establecimiento = direccion_establecimiento;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getDigito_verificador() {
		return digito_verificador;
	}

	public void setDigito_verificador(String digito_verificador) {
		this.digito_verificador = digito_verificador;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}

	public String getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public String getEmail_contacto() {
		return email_contacto;
	}

	public void setEmail_contacto(String email_contacto) {
		this.email_contacto = email_contacto;
	}

	public String getRepresentante_nombre() {
		return representante_nombre;
	}

	public void setRepresentante_nombre(String representante_nombre) {
		this.representante_nombre = representante_nombre;
	}

	public String getRepresentante_paterno() {
		return representante_paterno;
	}

	public void setRepresentante_paterno(String representante_paterno) {
		this.representante_paterno = representante_paterno;
	}

	public String getRepresentante_materno() {
		return representante_materno;
	}

	public void setRepresentante_materno(String representante_materno) {
		this.representante_materno = representante_materno;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	public String getTelefono_contacto() {
		return telefono_contacto;
	}

	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}

	public String getInstitucion() {
		return institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getCentro_laboral() {
		return centro_laboral;
	}

	public void setCentro_laboral(String centro_laboral) {
		this.centro_laboral = centro_laboral;
	}
	
}
