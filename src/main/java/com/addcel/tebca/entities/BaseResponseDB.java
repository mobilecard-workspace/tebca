package com.addcel.tebca.entities;

public class BaseResponseDB {
	
	private int idError;
	private String mensajeError;
	
	public BaseResponseDB() {
		// TODO Auto-generated constructor stub
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

}
