package com.addcel.tebca.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TEMPLATE_EMAIL")
public class TEMPLATE_EMAIL {

	@Id
    @Column(name = "ID")
	private String ID;
	
	@Column(name = "ASUNTO")
	private String ASUNTO;
	
	@Column(name = "CUERPO")
	private String CUERPO;
	
	@Column(name = "ID_APP")
	private int ID_APP;
	
	@Column(name = "IDIOMA")
	private String IDIOMA;
	
	public TEMPLATE_EMAIL() {
		// TODO Auto-generated constructor stub
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getASUNTO() {
		return ASUNTO;
	}

	public void setASUNTO(String aSUNTO) {
		ASUNTO = aSUNTO;
	}

	public String getCUERPO() {
		return CUERPO;
	}

	public void setCUERPO(String cUERPO) {
		CUERPO = cUERPO;
	}

	public int getID_APP() {
		return ID_APP;
	}

	public void setID_APP(int iD_APP) {
		ID_APP = iD_APP;
	}

	public String getIDIOMA() {
		return IDIOMA;
	}

	public void setIDIOMA(String iDIOMA) {
		IDIOMA = iDIOMA;
	}
	
}
